# Downloader for Instagram

_**Please note:** Downloading 1300 post accounts works but 1400 post accounts doesn't (see memory issues below)._

Beta version (work in progress)

View the tags section of this git repository (I use gitlab), to see the considerable version history.

This was originally created by someone else but they've abandoned it since 2021 so I'm the new maintainer.

## How to install
It's not on the extension directory, obviously, as it's not finished. You have to install it manually by _sideloading_ an _unpacked_ extension.

* [Installation instructions for Microsoft Edge](https://learn.microsoft.com/en-us/microsoft-edge/extensions-chromium/getting-started/extension-sideloading)
* [For Firefox](https://extensionworkshop.com/documentation/develop/temporary-installation-in-firefox/)

## Features to-do

* Fetch all images within the carousel, when downloading a posts
* Acknowledge instagram's user verification challenge, to prompt the user to access it, if need be]

## Usability to-do

* Show that processing 417 posts takes 1 hour
* Improve the design
* Show the number of posts recently processed, on the loading screen

## Polish up to-do
* Clean up the coding style (to make it easier to read for me)
* Block the incessant performance draining bot-detecting continuous pings (in case your adblocker isn't already doing it)

## Postponed (exceeded memory issues)
* Fix the memory leak (that occurs after using a batch import on a 1200+ post account, making browsing slow)
* Fix the memory leak that allows creating 1300 post zip files to work whilst abruptly failing 1400 post zip files, whilst producing zero javascript errors, it just exits the for/while loop.


## Contributor Advice
The throttling in the extension that makes the user wait before fetching or downloading the next post (or image), is there for a _reason_. Yes I have tested it from trial and error, so there's no need to make it any shorter. 

That's unless you want to be asked to provide a selfie to instagram with you signing a piece of paper with today's date, or maybe even using facial recognition to scan your face on your smartphone's camera and you submitting your ID documents.
