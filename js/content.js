(function() {
    var cssPrefix = 'inst_dl_simple_';
    var gb = window.simpleGb;
    var Extension = {
        dlBtnClassName: `${cssPrefix}dl_btn`,
        authorPageSector: null,
        cachedMediaIdentifiers: [],
        cachedMedia: {
            main: [],
            tagged: [],
            saved: [],
            igtv: [],
        },
        loaderBtnState: {
            on: function(el) {
                $(`.${cssPrefix}icon`, el).addClass(`${cssPrefix}loader`);
            },
            off: function() {
                $(`.${Extension.dlBtnClassName} .${cssPrefix}icon.${cssPrefix}loader`).removeClass(`${cssPrefix}loader`);
                $(`.${Extension.Stories.storyDlBtnClassName} .${cssPrefix}icon.${cssPrefix}loader`).removeClass(`${cssPrefix}loader`);
            }
        },
        Stories: {
            storyDlBtnClassName: `${cssPrefix}story_dl_btn`,
            storyBulkDlBtnClassName: `${cssPrefix}story_bulk_dl_btn`,
            appendStoryDlBtn: function(container) {
                var $parent = $(container);
                if ($parent.find(`.${cssPrefix}story_dl_btn_wrap`).length === 0) {
                    $parent.append(`<div class="${cssPrefix}story_dl_btn_wrap">
      <a class="${this.storyDlBtnClassName}">
       <span class="${cssPrefix}icon"></span>
       <span class="${cssPrefix}text">${chrome.i18n.getMessage('download')}</span>
      </a>
      <a class="${this.storyBulkDlBtnClassName}">
       <span class="${cssPrefix}icon"></span>
       <span class="${cssPrefix}text">${chrome.i18n.getMessage('download_all')}</span>
       <span class="stories_count"></span>
      </a>
     </div>`);
                    this.toggleVisibleStoriesButtons(container);
                }
            },
            toggleVisibleStoriesButtons: function(container) {
                var dlAllBtn = document.querySelector(`.${this.storyBulkDlBtnClassName}`);
                if (!dlAllBtn) {
                    return;
                }
                if (window.location.pathname.match(new RegExp('stories\\/(locations|tags)\\/([^/]+)'))) {
                    dlAllBtn.style.display = 'none';
                    return;
                }
                var countMedias = 1;
                var _mviq1 = container.querySelector('section header > div:first-child');
                if (_mviq1) {
                    countMedias = _mviq1.childElementCount;
                }
                if (countMedias > 1) {
                    dlAllBtn.querySelector('.stories_count').innerText = '(' + countMedias + ')';
                    dlAllBtn.style.display = 'flex';
                } else {
                    dlAllBtn.style.display = 'none';
                }
            },
            onClickDownloadSingleStory: function(e) {
                var el = this,
                    url, filename, container = el.parentNode.parentNode.querySelector('section');
                if (!container) {
                    return;
                }
                url = gb.getLinkFromHtml(container)
                if (url) {
                    var userName = window.location.pathname.match(new RegExp('stories\\/([^/]+)'));
                    userName = userName && userName[1];
                    filename = gb.getFileNameFromLink(url, userName);
                    Extension.whenGetPhotoVideoInfo({
                        url: url,
                        filename: filename || null,
                    });
                }
            },
            onClickBulkDownloadStories: function(e) {
                this.bulkCanceled = false;
                Extension.loaderBtnState.on(e.target);
                this.getAllLinksShowingStories(function(res, userName) {
                    Extension.loaderBtnState.off();
                    if (res && res.length) {
                        Extension.bulkDownloadModule.downloadZIP({
                            files: res,
                            filename_prefix: userName,
                            is_story: true,
                        }, function() {});
                    }
                });
            },
            getAllLinksShowingStories: function(callback) {
                if (window.location.pathname.match(new RegExp('stories\\/(locations|tags)\\/([^/]+)'))) {
                    return;
                }
                var self = this;
                var highlightsId = window.location.pathname.match(new RegExp('stories\\/highlights\\/([^/]+)'));
                if (highlightsId && highlightsId[1]) {
                    return gb.sendMessage({
                        action: 'requestHighlightStories',
                        highlight_reel_id: highlightsId[1]
                    }, function(res) {
                        var key = 'highlight:' + highlightsId[1];
                        if (!res || res.error || !res.reels || !res.reels[key]) {
                            return callback();
                        }
                        var userName = res.reels[key].user && res.reels[key].user.username;
                        callback(self.prepareStories(res.reels[key].items, res.reels[key].user), userName);
                    })
                }
                var userName = window.location.pathname.match(new RegExp('stories\\/([^/]+)'));
                if (userName && userName[1]) {
                    return this.getUserStories(userName[1], callback);
                }
                callback();
            },
            getUserStories: function(userName, callback) {
                var self = this;
                gb.requestAuthorInfo(userName, function(res) {
                    try {
                        var userId = res.graphql.user.id;
                    } catch (e) {
                        return callback();
                    }
                    gb.sendMessage({
                        action: 'requestUserStories',
                        user_id: userId
                    }, function(res) {
                        if (!res || !res.items || !res.items.length) {
                            return callback();
                        }
                        callback(self.prepareStories(res.items), userName);
                    });
                });
            },
            prepareStories: function(stories, userData) {
                var result = [];
                stories.forEach(function(story) {
                    var src = null;
                    var maxWidth = 0;
                    var maxHeight = 0;
                    if (story.video_versions) {
                        story.video_versions.forEach(function(story) {
                            if (story.width > maxWidth) {
                                maxWidth = story.width;
                                maxHeight = story.height;
                                src = story.url;
                            }
                        });
                    } else {
                        story.image_versions2.candidates.forEach(function(story) {
                            if (story.width > maxWidth) {
                                maxWidth = story.width;
                                maxHeight = story.height;
                                src = story.url;
                            }
                        });
                    }
                    var filename = src.match(new RegExp('\\/([^/?]+)(?:$|\\?)'));
                    filename = filename && filename[1];
                    if (!filename) {
                        filename = null;
                    } else {
                        var userName = story.user.username || (userData && userData.username) || '';
                        if (userName.length) {
                            filename = `${userName}_${filename}`
                        }
                    }
                    var itemMedia = {
                        url: src,
                        filename: filename,
                        username: userName,
                    };
                    result.push(itemMedia);
                });
                return result;
            },
        },
        bulkDownloadModule: {
            bulkCanceled: false,
            bulkButtonClassName: `${cssPrefix}bulk_download_btn`,
            PT_DEFAULT: 1,
            PT_USER_PAGE: 2,
            pageType: null,
            processIsActive: false,
            processId: null,
            insertBulkDownloadButton: function() {
                if (document.querySelector(`.${this.bulkButtonClassName}`)) return;
                var container = document.querySelector('._aa_c');
                //if (container && (container.querySelectorAll('a[href*="/direct/inbox/"], a[href*="/explore/"], a[href*="/accounts/activity/"]').length > 2 || container.querySelector('a[href*="/accounts/login/"]'))) {
                    $(`<div class="${cssPrefix}bulk_download_btn_wrap"><div class="${this.bulkButtonClassName}"></div></div>`).appendTo(container);
                //}
            },
            showBulkDownloadMenu: function(button) {
                var $bulkDownloadMenu = $(`
<div class="${cssPrefix}bulk_download_menu_wrap">
<div class="${cssPrefix}bulk_download_menu_dialog"></div>
<div class="${cssPrefix}bulk_download_menu_loader"><span class="${cssPrefix}icon"></span></div>
</div>
`).appendTo(button.parentNode.parentNode);
                $(`.${cssPrefix}bulk_download_menu_dialog`, $bulkDownloadMenu).on('click', function() {
                    $bulkDownloadMenu.remove();
                });
                var self = this;
                gb.sendMessage('isBulkDownloadProcessNow', function(res) {
                    if (res) {
                        return self.fillBulkDownloadMenu({
                            header: chrome.i18n.getMessage('bulk_parallel_forbidden'),
                        });
                    }
                    if (!document.querySelector('header a[href*="followers"]')) {
                        self.pageType = self.PT_DEFAULT;
                        self.buildBulkDownloadMenu({
                            count: Extension.cachedMediaIdentifiers.length
                        });
                    } else {
                        if (Extension.getAuthorPageSector() == 'igtv') {
                            return self.fillBulkDownloadMenu({
                                header: chrome.i18n.getMessage('bulk_igtv_forbidden'),
                            })
                        }
                        var options = {
                            fromSaved: Extension.getAuthorPageSector() == 'saved',
                            fromTagged: Extension.getAuthorPageSector() == 'tagged',
                        };
                        self.checkVirtualScroll(options, function(res) {
                            if (!res || !res.count) {
                                self.pageType = self.PT_DEFAULT;
                                self.buildBulkDownloadMenu({
                                    count: Extension.cachedMediaIdentifiers.length
                                });
                            } else {
                                options.count = res.count;
                                options.user_page = true;
                                self.pageType = self.PT_USER_PAGE;
                                self.buildBulkDownloadMenu(options);
                            }
                        });
                    }
                });
            },
            fillBulkDownloadMenu: function(options) {
                $(`.${cssPrefix}bulk_download_menu_loader`).remove();
                $(`.${cssPrefix}bulk_download_menu_wrap`).append(`
<div class="${cssPrefix}bulk_download_menu" style="    padding: 8px;
    box-sizing: content-box;
    margin-top: 136px;
    border: 3px black;
    border-style: double;">
        <!-- the added style only works inline, not external -->
<div class="${cssPrefix}bulk_download_menu_header">${options.header || ''}</div>
<div class="${cssPrefix}bulk_download_menu_content">${options.download || ''}</div>
<div class="${cssPrefix}bulk_download_menu_footer">${options.footer || ''}</div>
</div>
`)
            },
        buildBulkDownloadMenu: function(options) {
                var self = Extension.bulkDownloadModule;
                if (options.count === 0) {
                    return this.fillBulkDownloadMenu({
                        header: chrome.i18n.getMessage('links_not_found'),
                    });
                }
                var headerHtml = '',
                    footerHtml = '',
                    linksWrapperHtml = '';
                if (options.fromTagged) {
                    var pageName = $('a[href*="/tagged/"]').text();
                    if (pageName.length) {
                        pageName = (' (' + pageName + ')').toUpperCase();
                    }
                    headerHtml = `<span> ${chrome.i18n.getMessage('download_all')}&nbsp;${pageName}</span>
<span class="igtv_skip_sign">
<img src="${chrome.runtime.getURL("/img/info.svg")}">
</span>`;
                } else {
                    var maxCount = options.count;
                    headerHtml = `
<span>${chrome.i18n.getMessage('files_found_on_page')}:&nbsp;</span> 
<span class="files_found_count">${maxCount}</span>
<span class="igtv_skip_sign">
<img src="${chrome.runtime.getURL('/img/info.svg')}">
</span>
`;
                    var defaultEndCount = maxCount;
                    var maxValForStart = (parseInt(maxCount) - 1).toString();
linksWrapperHtml += `
<div class="${cssPrefix}set_range_form">
      <span>${chrome.i18n.getMessage('set_range')}</span>
      <span style="margin-left: -3px">${chrome.i18n.getMessage('from')}</span> 
      <input id="${cssPrefix}range_start" type="number" min="1" max="${maxValForStart}" value="1">
      <span>${chrome.i18n.getMessage('to')}</span>
      <input id="${cssPrefix}range_end" type="number" min="2" value="${defaultEndCount}" max="${maxCount}">
</div> <!-- end .set_range form FIELD -->

<div class="${cssPrefix}set_millisecdelay_form" style="flex-direction: row;">    
    throttle in milliseconds
    <input id="${cssPrefix}millisecdelay" type="number" min="2" value="500">
</div>
<div style="font-size: 0.85em;color: #1c1c1c;flex-direction: row;">
    1000ms = 1 second<br>applied between each image fetch
    <br>500ms is recommended
</div>
`;
                    if (self.pageType == self.PT_DEFAULT) {
                        footerHtml = chrome.i18n.getMessage('scroll_page_for_download_more');
                    }
                }
                linksWrapperHtml += `<div class="${cssPrefix}menu_button_wrap">
<div class="${cssPrefix}bulk_menu_button">${chrome.i18n.getMessage('download')}</div>
</div>`;
                



    this.fillBulkDownloadMenu({
        header: headerHtml,
        download: linksWrapperHtml,
        footer: footerHtml
    });
                if (options.fromTagged) {
                    document.querySelector(`.${cssPrefix}bulk_menu_button`).dataset.from_tagged = '1';
                } else {
                    var inputStart = document.querySelector(`#${cssPrefix}range_start`);
                    var inputEnd = document.querySelector(`#${cssPrefix}range_end`);
                    var $inputEnd = $(inputEnd);
                    var $inputStart = $(inputStart);
                    $inputEnd.on('keydown', function() {
                        var el = this;
                        var actualMaxCount = this.getAttribute('max');
                        setTimeout(function() {
                            var val = parseInt(el.value);
                            if (isNaN(val) || val < inputStart.value) {
                                el.value = inputStart.value;
                            } else if (val > actualMaxCount) {
                                //el.value = actualMaxCount;
                            }
                        }, 1000);
                    });
                    $inputStart.on('keydown', function() {
                        var el = this;
                        setTimeout(function() {
                            var val = parseInt(el.value);
                            if (isNaN(val) || val < 1) {
                                el.value = 1;
                            } else if (val > inputEnd.value) {
                                el.value = inputEnd.value;
                            }
                        }, 1000);
                    });
                    $inputEnd.on('focus', function() {
                        this.select();
                    });
                    $inputStart.on('focus', function() {
                        this.select();
                    });
                }
            },
            //start the bulk download task / major task start
            bulkDownloadStart: function() {
                let millisecdelay = $(`#${cssPrefix}millisecdelay`).val();
                let userName = gb.getUserNameFromWindowLocation();
                // console.log("html millisecdelay is "+millisecdelay);

                Extension.bulkDownloadModule.bulkCanceled = false;
                var count, typeDownload;
                if (Extension.bulkDownloadModule.pageType == Extension.bulkDownloadModule.PT_USER_PAGE) {
                    var start = $(`#${cssPrefix}range_start`).val();
                    var end = $(`#${cssPrefix}range_end`).val();
                    // 1/3 fetch the values from the form fields
                    count = end - start;
                    typeDownload = 2;
                    //console.log("userName is "+userName);
                    //type 2 is downloading from "tagged" or "saved" page
                } else {
                    start = $(`#${cssPrefix}range_start`).val();
                    end = $(`#${cssPrefix}range_end`).val();
                    count = end - start;
                    typeDownload = 1;
                    // console.log("userName is "+userName);
                    //type 1 download is downloading from profile "posts" initial page
                }
                if (isNaN(start)) {
                    start = 1
                }
                if (isNaN(end)) {
                    end = 1
                }
                var downloadProcessFunc = function() {
                    if (typeDownload === 1) {
                        var identifiers = Extension.getFoundIdentifiers(start, end);
                        // 1/2 downloadByIdentifiers ---> 2/2 prepareLinksByIdentifiers


                        // var cyclecount = 656;
                        var totalpostcount = identifiers.length;
                        // console.log ("cyclecount in bulkDownloadStart: is "+cyclecount);
                        // console.log ("totalpostcount in bulkDownloadStart: is "+totalpostcount);
                                //simpleGb.sleep(500); console.log("ggggg");

                        Extension.bulkDownloadModule.downloadByIdentifiers({
                            identifiers: identifiers,
                            progress_bar_start: null,
                            filename_prefix: userName,
                            millisecdelay: millisecdelay
                        });
                                       //simpleGb.sleep(500); console.log("no need ever");
                        //console.log("bdm type 1 millisecdelay is "+millisecdelay);
                    } else if (typeDownload === 2) {
                        var fromTagged = Extension.getAuthorPageSector() == 'tagged';
                        var fromSaved = Extension.getAuthorPageSector() == 'saved';
                        var opts = {
                            start: start,
                            end: end,
                            form_saved: fromSaved,
                            form_tagged: fromTagged,
                            millisecdelay: millisecdelay,
                            totalpostcount: totalpostcount
                        };

                        //simpleGb.sleep(500); console.log("no need ever");
 
                            // 2/3 pass form fields to the below function
                            // 1/6 downloadByIdentifiers ---> 2/6 prepareLinksByIdentifiers
                            //console.log("bdm type 2 millisecdelay is "+millisecdelay);
                        Extension.bulkDownloadModule.bulkDownloadFromAuthorPage(opts, function(identifiers, progressBarStart, userName) {
                            // 3/6 start the bulk download
                            // 4/6 locatePostsForMedia() -----> 5/6 fetchLocatedMediaContent()
                            // 5/6 downloadProcessFunc()
                            // 6/6 locatePostsForMedia() -----> downloadZip()
                            // ??? bulkDownloadFromAuthorPage

                            var filenamePrefix = userName + (fromSaved ? '_saved' : (fromTagged ? '_tagged' : ''));
                            Extension.bulkDownloadModule.downloadByIdentifiers({
                                identifiers: identifiers,
                                progress_bar_start: progressBarStart,
                                filename_prefix: filenamePrefix,
                                form_saved: fromSaved,
                                form_tagged: fromTagged,
                                millisecdelay: millisecdelay
                            });
                        },
                            millisecdelay
                        );
                    } else {
                        return;
                    }
                    Extension.bulkDownloadModule.processIsActive = true;
                };
                downloadProcessFunc();
                $(`.${cssPrefix}bulk_download_menu`).parent().remove();
            },
            // goseekmilliseekdelay: function(){ return millisecdelay; },
            checkVirtualScroll: function(opts, callback) {

                var userName = gb.getUserNameFromWindowLocation();
                if (!userName) {
                    var userTitleWrap = document.querySelector('main header section h2');
                    userName = userTitleWrap && userTitleWrap.innerText;
                }
                if (!userName) {
                    return callback({
                        error: 1
                    });
                }
                opts.userName = userName;
                gb.getUserFirstPagePostsData(opts, function(res) {
                    if (!res || res.error || typeof res.userId == 'undefined') {
                        return callback({
                            error: 1
                        });
                    }
                    if (!opts.fromTagged && (typeof res.end_cursor == 'undefined' || typeof res.has_next_page == 'undefined' || typeof res.count == 'undefined' || typeof res.identifiers == 'undefined')) {
                        return callback({
                            error: 1
                        });
                    }
                    var count = res.count;
                    var endCursor = res.end_cursor;
                    var userId = res.userId;
                    Extension.bulkDownloadModule.currentPageUserId = userId;
                    if (res.has_next_page === false) {
                        return callback({
                            success: 1,
                            count: count
                        });
                    }
                    gb.getPostsDataByQueryHash({
                        end_cursor: endCursor,
                        user_name: userName,
                        user_id: userId,
                        touch: true,
                        from_saved: opts.fromSaved,
                        from_tagged: opts.fromTagged,
                        first: 12,
                    }, function(res) {
                        if (!res || res.error || typeof res.end_cursor == 'undefined' || typeof res.has_next_page == 'undefined' || typeof !res.identifiers == 'undefined') {
                            return callback({
                                error: 1
                            });
                        }
                        if (opts.fromTagged) {
                            count = res.has_next_page === false ? res.count : 'unknown';
                        }
                        return callback({
                            success: 1,
                            count: count
                        });
                    });
                });
            },
            bulkDownloadFromAuthorPage: function(options, callback) {
                var identifiers = [],
                    endCursor = null,
                    userId = null,
                    hasNextPage = true,
                    timeEnd = false,
                    self = this,
                    fromSaved = options.form_saved,
                    fromTagged = options.form_tagged,
                    start = options.start,
                    end = options.end,
                    allCount = end,
                    millisecdelay = options.millisecdelay;
                this.processId = Math.random();
                if (fromTagged) {
                    var progressBarInfo = {
                        from_tagged: true,
                        counter_text: identifiers.length,
                    };
                } else {
                    var progressBarGravity = 0.6;
                    var oneValuePercent = (100 / allCount) * progressBarGravity;
                    progressBarInfo = {
                        from_tagged: fromTagged,
                        allCount: allCount,
                        maxValue: progressBarGravity * 100,
                        oneValuePercent: oneValuePercent,
                    };
                }
                var modalX = Extension.modalInit();
                modalX.prepareDownload({
                    from_tagged: fromTagged,
                }, {
                    cancelCallback: function() {
                        self.bulkCanceled = true;
                        Extension.bulkDownloadModule.processIsActive = false;
                    }
                });
                var userName = gb.getUserNameFromWindowLocation();
                if (!userName) {
                    var userTitleWrap = document.querySelector('main header section h2');
                    userName = userTitleWrap && userTitleWrap.innerText;
                }
                if (!userName) {
                    Extension.bulkDownloadModule.processIsActive = false;
                    return modalX.showErrorBox(chrome.i18n.getMessage('errorUnsupportedWebpage'));
                }
                userId = Extension.bulkDownloadModule.currentPageUserId;
                var scrolledPagesCount = 0;
                gb.firstParamRequestJSONgraphqlQuery = 48;
                var firstRequestDone = false;

                function scrollPage() {
                    if (self.bulkCanceled) {
                        return;
                    }
                    if ((!fromTagged && end && identifiers.length >= end) || !hasNextPage || (timeEnd && !fromTagged)) {
                        if (!fromTagged && start && end) {
                            identifiers = identifiers.slice(start - 1, end);
                        }
                        var progressBarStart = progressBarGravity * 100;
                        return callback(identifiers, progressBarStart, userName);
                    }
                    if ((!fromTagged && (firstRequestDone && !endCursor)) || !userId) {
                        Extension.bulkDownloadModule.processIsActive = false;
                        return modalX.showErrorBox(chrome.i18n.getMessage('errorZipNotKnownInScroll'));
                    }
                                            //simpleGb.sleep(500); console.log("no need ever");
                    var needBackUsualModalBoxFooter = false;
                    var waitTimeout = setTimeout(function() {
                        progressBarInfo.no_progress = true;
                        progressBarInfo.dont_panic = true;
                        needBackUsualModalBoxFooter = true;
                        modalX.updatePrepareDownload(progressBarInfo);
                    }, 20000);

                    //console.log("dl sngl file at stage 3");
                    //simpleGb.sleep(500);


                    gb.getPostsDataByQueryHash({
                        end_cursor: endCursor,
                        user_name: userName,
                        user_id: userId,
                        from_saved: fromSaved,
                        from_tagged: fromTagged,
                        bulkDownloadObject: Extension.bulkDownloadModule,
                        process_id: self.processId,
                    }, function(res) {
                        firstRequestDone = true;
                        if (Extension.bulkDownloadModule.bulkCanceled) {
                            return;
                        }
                        clearTimeout(waitTimeout);
                        if (!res || res.error) {
                            Extension.bulkDownloadModule.processIsActive = false;
                            return modalX.showErrorBox(chrome.i18n.getMessage('errorZipNotKnownInScroll'));
                        }
                        if (needBackUsualModalBoxFooter) {
                            progressBarInfo.no_progress = false;
                            progressBarInfo.dont_panic = false;
                            needBackUsualModalBoxFooter = false;
                        }
                        endCursor = res.end_cursor;
                        hasNextPage = res.has_next_page;
                        timeEnd = res.time_end;
                        identifiers = identifiers.concat(res.identifiers);
                        if (fromTagged) {
                            progressBarInfo = {
                                from_tagged: true,
                                counter_text: identifiers.length,
                            };
                                                                                simpleGb.sleep(500); console.log("no need ever");
                        } else {
                                                    simpleGb.sleep(500); console.log("no need ever");
                            progressBarInfo.oneValuePercent = oneValuePercent * (res.identifiers.length || 1);
                        }
                        modalX.updatePrepareDownload(progressBarInfo);
                        scrolledPagesCount++;
                        if (scrolledPagesCount < 50) {
                            var timeout = 500 + 1000;
                        } else if (scrolledPagesCount < 100) {
                            timeout = 1000 + 1000;
                        } else if (scrolledPagesCount < 200) {
                            timeout = 2000 + 1000;
                        } else {
                            timeout = 3000 + 1000;
                        }
                        setTimeout(function() {
                            scrollPage();
                        }, timeout);
                    });
                }
                scrollPage();
            },
            downloadByIdentifiers: function(opts) {
                var identifiers = opts.identifiers,
                progressBarStart = opts.progress_bar_start;
                millisecdelay = opts.millisecdelay;

                                                                    //simpleGb.sleep(500); console.log("no need ever");
                    
                Extension.bulkDownloadModule.prepareLinksByIdentifiers(identifiers, progressBarStart, function(links) {
                                            //start fetching each individual post permalink
                    //console.log("the filename prefix is "+opts.filename_prefix);
                    
                    if (links && links.length) { //if all files designated for the zip file are already previously downloaded....
                        Extension.bulkDownloadModule.downloadZIP({ //go to the next stage
                            files: links,
                            filename_prefix: opts.filename_prefix,
                            millisecdelay: opts.millisecdelay
                        });
                    } else if (Extension.bulkDownloadModule.bulkCanceled) {} else {
                        Extension.bulkDownloadModule.processIsActive = false;
                        Extension.modalInit().showErrorBox(chrome.i18n.getMessage('httpError409'));
                    }
                }
                , opts.millisecdelay
                  //^^ pass the variable within the function
                // 1/3 bulkDownloadStart
                // ---> 2/3 downloadByIdentifiers
                // ---> ---> 3/3 prepareLinksByIdentifiers
                )
                //console.log(opts.millisecdelay);
            },
            prepareLinksByIdentifiers: function(identifiers, progressBarStart, callback, millisecdelay) {
                // console.log(Object.keys(identifiers));
                // console.log("millisecdelay within prepareLinksByIdentifiers() is "+millisecdelay);
                //console.log("millisecdelay is "+millisecdelay);

                // simpleGb.sleep(9000);
                // console.log("wait for "+millisecdelay+"ms before fetching next media item");

                if (!identifiers || !identifiers.length) {
                    return callback();
                }
                
                sessionStorage.setItem('cyclecount', 1);

                            //simpleGb.sleep(500); console.log("no need ever");

                progressBarStart = progressBarStart || 0;
                var progressBarGravity = (100 - progressBarStart) / 100;
                var allCount = identifiers.length;
                var self = this;
                var links = [];
                var modalX = Extension.modalInit();
                var _429 = false;
                var _429TooLong = false;
                var progressBarInfo = {
                    allCount: allCount,
                    oneValuePercent: (100 / allCount) * progressBarGravity,
                };
                modalX.prepareDownload({
                    bar_start: progressBarStart,
                }, {
                    cancelCallback: function() {
                        self.bulkCanceled = true;
                        Extension.bulkDownloadModule.processIsActive = false;
                    }
                });

               simpleGb.sleep(500); console.log("no need ever");

                if (millisecdelay <= 500){ millisecdelay2 = 2500; }
                //millisecdelay2 is unused but millisecdelay is supposed to be there
                locatePostsForMedia(identifiers, null, millisecdelay);
                //process one file at a time
                //desbest edit

                function fetchLocatedMediaContent(resolve, reject, rejectedIdentifiers, postIdentifier, tryNoInternal, millisecdelay, cyclecount, totalpostcount) {

                    //console.log("millisecdelay within fetchLocatedMediaContent() is "+millisecdelay);
                    //simpleGb.sleep(500);
                    // console.log("totalpostcount is "+totalpostcount);
                    var postQueueKey = Object.keys(postIdentifier);
                    if (self.bulkCanceled) {
                        return reject();
                    }
                    if (typeof postIdentifier == 'object') {
                        // console.log(postIdentifier);
                        // console.log(Object.keys(postIdentifier));
                        // console.log(Object.values(postIdentifier));

                                       simpleGb.sleep(500); console.log("jjjjj");

                        if (postIdentifier.url) {
                            links.push(postIdentifier);
                            modalX.updatePrepareDownload(progressBarInfo);
                            resolve(postIdentifier);
                            return;
                        } else {
                            postIdentifier = postIdentifier.identifier;
                        }
                                       simpleGb.sleep(500); console.log("kkkkk");
                    }
                    if (_429TooLong) {
                        return;
                    }
                    if (_429 && !tryNoInternal) {
                        setTimeout(function() {
                            fetchLocatedMediaContent(resolve, reject, rejectedIdentifiers, postIdentifier, tryNoInternal, millisecdelay, cyclecount, totalpostcount);
                        }, 20 * 1000);
                        return;
                    }

                    // console.log("millisecdelay is "+millisecdelay);
                    //console.log("totalpostcount is "+totalpostcount);
                    // console.log(Object.keys(postIdentifier));
                    // console.log(Object.values(postIdentifier));

                    // simpleGb.sleep(9000);
                    // console.log("wait for "+millisecdelay+"ms before fetching next media item");

                    //////
                    //use ajax to fetch the post
                    //simple_gb.js Line 79

                    gb.getWholePostData({
                        postIdentifier: postIdentifier,
                        cyclecount: postQueueKey,
                        totalpostcount: totalpostcount,

                    }, function(res) {
                        if (res && res.error && res.reason == "botchallenge") {
                            Extension.modalInit().showErrorBox(chrome.i18n.getMessage('botchallenge'+"<br>"+botchallengeurl));
                        }
                        ////////////////////
                        //begin http error 429
                        // it is not always the end :S
                        //  if it fails to fetch ONE post the first time, then ideally
                        //      we want to fetch it again a few more times, as the
                        //      user's computer or website might have 
                        //      weirdly sporadically timed out as a blip
                        if (res && res.error && res.reason == 429) {
                            if (!_429) {
                                _429 = true;
                                tryNoInternal = 1;
                                progressBarInfo.no_progress = true;
                                progressBarInfo.dont_panic = true;
                                modalX.updatePrepareDownload(progressBarInfo);
                                setTimeout(function() {
                                    fetchLocatedMediaContent(resolve, reject, rejectedIdentifiers, postIdentifier, tryNoInternal, millisecdelay);
                                }, 20 * 1000);
                                return;
                            } else {
                                if (tryNoInternal) {
                                    if (++tryNoInternal < 2) {
                                        setTimeout(function() {
                                            fetchLocatedMediaContent(resolve, reject, rejectedIdentifiers, postIdentifier, tryNoInternal, millisecdelay);
                                        }, tryNoInternal * 20 * 1000);
                                        return;
                                    } else {
                                        _429TooLong = true;
                                        reject();
                                        locatePostsForMedia(rejectedIdentifiers);
                                        return;
                                    }
                                } else {
                                    setTimeout(function() {
                                        fetchLocatedMediaContent(resolve, reject, rejectedIdentifiers, postIdentifier, tryNoInternal, millisecdelay, cyclecount, totalpostcount);
                                    }, 10 * 1000);
                                    return;
                                }
                            }
                        }
                        //end http error 429
                        //////////////////
                        if (!res || res.error || !res.length) {
                            rejectedIdentifiers.push(postIdentifier);
                            resolve(postIdentifier);
                            return;
                        }
                        if (_429) {
                            _429 = false;
                            progressBarInfo.no_progress = false;
                            progressBarInfo.dont_panic = false;
                        }
                        res.forEach(function(item) {
                            if (item.product_type == "igtv") {
                                return;
                            }
                            links.push(item);
                        });
                        modalX.updatePrepareDownload(progressBarInfo);
                        resolve(postIdentifier);
                    },{
                        millisecdelay: millisecdelay
                    }
                    //desbest edit
                    );
                }

                function locatePostsForMedia(localIdentifiers, tryNo, millisecdelay) {
                    // console.log("millisecdelay within locatePostsForMedia() is "+millisecdelay);
                    //var millisecdelayForProcess = millisecdelay;
                    var millisecdelayForProcess = 500;
                    var totalpostcount = localIdentifiers.length;
                    // console.log(Object.keys(localIdentifiers));
                    // console.log(Object.values(localIdentifiers));
                    tryNo = tryNo || 0;
                    tryNo++;
                    
                    //////--->max attempts exceeded
                    var maxAttempts = 3;
                    if (tryNo > maxAttempts) {

                        var addedFromCacheCount = 0;
                        localIdentifiers.forEach(function(postIdentifier) {
                            if (typeof postIdentifier == 'object') {
                                postIdentifier = postIdentifier.identifier;
                            }
                            var link = Extension.getCachedLinkByIdentifier(postIdentifier);
                            if (link) {
                                links.push(link);
                                addedFromCacheCount++;
                            }
                        });
                        if ((localIdentifiers.length - addedFromCacheCount) > 5 && localIdentifiers.length > ((allCount / 10) + addedFromCacheCount)) {
                            return callback({
                                error: 1
                            });
                        } else {
                            callback(links);
                        }
                        return;
                    } else if (_429TooLong) {
                        if ((allCount * 0.9) > links.length) {
                            return callback({
                                error: 1
                            });
                        } else {
                            callback(links);
                        }
                        return;
                    }
                    //////--->max attempts exceeded

                    // console.log(Object.keys(localIdentifiers));
                    // console.log(Object.values(localIdentifiers));
                    // remainder = localIdentifiers.length - addedFromCacheCount

                    var rejectedIdentifiers = [];
                    gb.execTasksFlow({
                        data: localIdentifiers,
                        asyncCount: 12 / tryNo,
                    }, function(postIdentifier, resolve, reject, requestMediaItemCount, localIdentifiers) {
                        // console.log("pi-l for postIdentifier is "+postIdentifier.length);
                        // console.log("postIdentifier.keys for cyclecount is "+Object.keys(postIdentifier));
                        // console.log("postIdentifier.values for cyclecount is "+Object.keys(postIdentifier));
                        // console.log("postIdentifier is "+postIdentifier);
                        // console.log("li-l for postIdentifier is "+localIdentifiers.length);
                        // console.log("localIdentifiers.keys for cyclecount is "+Object.keys(localIdentifiers));
                        // console.log("localIdentifiers.values for cyclecount is "+Object.keys(localIdentifiers));
                        // console.log("localIdentifiers is "+localIdentifiers);
                                                // what is links array?
                                                // see Line 857 callback(links) for a clue
                                                //it's not postIdentifier[i]
                        //fill this in later
                        fetchLocatedMediaContent(resolve, reject, rejectedIdentifiers, postIdentifier, null, millisecdelayForProcess, null, totalpostcount);
                        
                        cyclecount = sessionStorage.getItem('cyclecount');
                        cyclecountPlusOne = parseInt(cyclecount) +1;
                        sessionStorage.setItem('cyclecount',  cyclecountPlusOne);
                        
                        // console.log("cyclecount is "+cyclecount);
                        //console.log("msd for process");
                        //simpleGb.sleep(millisecdelayForProcess); //desbest edit
                    }).thenOne(function success(result) {
                        if (self.bulkCanceled) {
                            return callback();
                        }
                        if (rejectedIdentifiers.length) {
                            setTimeout(function() {
                                locatePostsForMedia(rejectedIdentifiers, tryNo);
                            }, tryNo * 3000);
                        } else {
                            callback(links);
                        }
                    }, function reject(result) {
                        return callback();
                    });
                }
            },
            // downloadZip <---- downloadProcessFunc() <----
            downloadZIP: function(opts, callback) {
                var files = opts.files,
                    prefixName = opts.filename_prefix,
                    isStory = opts.is_story;
                    millisecdelay = opts.millisecdelay;
                var self = this;
                var zip = new JSZip();
                prefixName = prefixName || 'Instagram';
                var modalX = Extension.modalInit();
                var prepareFinished = false;
                var xhrsStatus = {
                    aborted: false,
                };
                var allCount = files.length;
                var progressBarInfo = {
                    allCount: allCount,
                    successCount: 0,
                    oneValuePercent: 100 / allCount,
                };
                var timeoutDefault = 60000;
                var modalBoxCallbacks = {
                    enoughCallback: enoughCallback,
                };
                if (!isStory) {
                    modalBoxCallbacks.cancelCallback = function() {
                        self.bulkCanceled = true;
                        xhrsStatus.aborted = true;
                        Extension.bulkDownloadModule.processIsActive = false;
                    };
                } else {
                    modalBoxCallbacks.cancelCallback = function() {
                        self.bulkCanceled = true;
                        xhrsStatus.aborted = true;
                    };
                };

                //console.log("fffff1"); simpleGb.sleep(500000); console.log("fffff2");

                modalX.showDownloadProcess({
                    //download process continues in getBinaryContent()
                    files_count: allCount,
                }, modalBoxCallbacks);

                function enoughCallback() {
                    if (prepareFinished) return;
                    self.bulkCanceled = true;
                    xhrsStatus.aborted = true;
                    zipFiles();
                }

                function zipFiles() {
                    prepareFinished = true;
                    if (!isStory) {
                        Extension.bulkDownloadModule.processIsActive = false;
                    }
                    if (progressBarInfo.successCount > 100) {
                        modalX.showZippingLoader({
                            cancelCallback: function() {
                                Extension.bulkDownloadModule.processIsActive = false;
                            }
                        });
                    }
                    zip.generateAsync({
                        type: "blob"
                    }, function(meta) {
                        if (meta.percent && Math.random() > 0.99 && progressBarInfo.successCount > 100) {
                            modalX.updateZippingLoader(parseInt(meta.percent));
                        }
                    }).then(function(content) {
                        var date = new Date();
                        var zipFileName = prefixName + ' instagram ' +
                            date.getFullYear() +
                            '-' + date.getMonth().toString().padStart(2, "0") +
                            '-' + date.getDate() +
                            ' ' + date.getHours() +
                            'h' + date.getMinutes() +
                            'm' + date.getSeconds() +
                            's.zip';
                            // https://stackoverflow.com/a/16344301
                            // Zero Padding a Date with JavaScript
                        saveAs(content, zipFileName);
                        modalX.close();
                        if (typeof callback == 'function') {
                            callback({
                                success: 1
                            });
                        }
                        gb.sendMessage({
                            action: 'downloadZip',
                        });
                    }, function() {
                        Extension.bulkDownloadModule.processIsActive = false;
                        return modalX.showErrorBox(chrome.i18n.getMessage('errorZipFinalSave'));
                    });
                }
                gb.execTasksFlow({ //search for execTasksFlow to find where I added the delay
                    data: files,
                    asyncCount: 12,
                }, function(file, resolve, reject) { // execTasksFlow(1/3)
                    if (self.bulkCanceled) {
                        xhrsStatus.aborted = true;
                        return reject();
                    }
                    JSZipUtils.getBinaryContent(file.url, function(err, data) {
                        if (err) {
                            resolve(file.filename);
                        } else {
                                           //simpleGb.sleep(500); console.log("mmmmm");
                            progressBarInfo.successCount++;
                            modalX.updateProgressBar(progressBarInfo);
                            zip.file(file.filename, data, {
                                binary: true
                            });
                                           //simpleGb.sleep(millisecdelay); console.log("stage 2/3 nnnnn");
                            resolve(file.filename);
                        }
                    }, xhrsStatus, timeoutDefault);
                }).thenOne(function() { // execTasksFlow(2/3)
                    if (self.bulkCanceled) {
                        return;
                    }
                    zipFiles();
                }, function() {         // execTasksFlow(3/3)
                    if (!isStory) {
                        Extension.bulkDownloadModule.processIsActive = false;
                    }
                    if (!self.bulkCanceled) {
                        modalX.showErrorBox(chrome.i18n.getMessage('errorZipMisc'));
                        if (typeof callback == 'function') {
                            callback({
                                error: 1
                            });
                        }
                    }
                });
            },
        },
        run: function() {
            $(document).ready(function() {
                gb.bridge({
                    func: function(cb) {
                        var isMobile = navigator.userAgent.toLowerCase().indexOf('mobile') > -1;
                        cb({
                            is_mobile: isMobile
                        });
                    },
                    cb: function(data) {
                        if (data && data.is_mobile) {
                            return;
                        }
                        gb.fixForeach();
                        Extension.observerDOMStart();
                        Extension.userActionsListenerStart();
                        Extension.messagesListenerStart();
                    }
                });
            });
        },
        observerDOMStart: function() {
            if (gb.isIFrame()) {
                return Extension.findEmbedMedia();
            }
            Extension.findMedia();
            Extension.findStories();
            Extension.bulkDownloadModule.insertBulkDownloadButton();
            setInterval(function() {
                Extension.findMedia();
                Extension.findStories();
                Extension.bulkDownloadModule.insertBulkDownloadButton();
                Extension.checkStoriesButtons();
            }, 2000);
        },
        findStories: function(needRepeat) {
            if (!gb.isStoriesActive()) {
                return false;
            }
            var storiesViewer = gb.getStoriesViewer();
            if (!storiesViewer) {
                if (needRepeat) {
                    setTimeout(Extension.findStories, 200);
                }
            } else if (storiesViewer && storiesViewer.dataset.simpleDone != '1') {
                storiesViewer.dataset.simpleDone = '1';
                Extension.Stories.appendStoryDlBtn(storiesViewer);
            }
            return true;
        },
        findMedia: function() {
            document.querySelectorAll(['section > main article > div > div > div a[href^="/p/"]', 'article > div > div > div a[href^="/p/"]', 'section div > div > div a[href^="/p/"]', ].join(',')).forEach(function(node) {
                if (!node.parentNode.dataset.simpleDone && node.querySelector('img')) {
                    Extension.appendDlBtn(node.parentNode, 2);
                }
            });
            if (location.href.indexOf('/channel/') > -1) {
                document.querySelectorAll('section div > div > div a[href^="/tv/"]').forEach(function(node) {
                    if (!node.parentNode.dataset.simpleDone) {
                        Extension.appendDlBtn(node, 5);
                    }
                });
            }
            document.querySelectorAll(['div > div > article > header + div > div > div', 'div > div > article header + div + div > div > div', ].join(',')).forEach(function(node) {
                if (node.parentNode.dataset.simpleDone > 0) {
                    return;
                }
                if (node.querySelector('ul > li > div > div > div')) {
                    return;
                }
                if (!node.querySelector('img') && !node.querySelector('video')) {
                    return;
                }
                Extension.appendDlBtn(node.parentNode, 1);
            });
            document.querySelectorAll(['div > div > article > header + div > div > div ul > li > div > div > div', 'div > div > article header + div + div > div > div ul > li > div > div > div', ].join(',')).forEach(function(node) {
                if (node.parentNode.dataset.simpleDone > 0) {
                    return;
                }
                if (!node.querySelector('img') && !node.querySelector('video')) {
                    return;
                }
                Extension.appendDlBtn(node.parentNode, 4);
            });
        },
        findEmbedMedia: function() {
            document.querySelectorAll('.Embed > .Header + div').forEach(function(node) {
                if (node.dataset.simpleDone > 0) {
                    return false;
                }
                Extension.appendDlBtn(node, 3);
            })
        },
        checkStoriesButtons: function() {
            if (!gb.isStoriesActive()) {
                return false;
            }
            var nativeStoriesViewer = gb.getStoriesViewer();
            if (nativeStoriesViewer) {
                this.Stories.toggleVisibleStoriesButtons(nativeStoriesViewer);
            }
        },
        userActionsListenerStart: function() {
            var $body = $('body');
            var lastButtonClickTime = Date.now();
            var isRepeatedClick = function(e) {
                e.preventDefault();
                e.stopPropagation();
                var now = Date.now();
                if (lastButtonClickTime + 500 > now) {
                    return true;
                }
                lastButtonClickTime = now;
                return false;
            };
            $body.on('click', '.' + Extension.dlBtnClassName, function(e) {
                if (Extension.disabledApp) {
                    return;
                }
                if (isRepeatedClick(e)) {
                    return;
                }
                Extension.onClickDownloadBtn.call(this, e);
            });
            $body.on('click', `.${Extension.bulkDownloadModule.bulkButtonClassName}`, function(e) {
                if (isRepeatedClick(e)) {
                    return;
                }
                Extension.bulkDownloadModule.showBulkDownloadMenu(this);
            });
            $body.on('click', `.${cssPrefix}bulk_menu_button`, Extension.bulkDownloadModule.bulkDownloadStart);
            $body.on('click', `.${Extension.Stories.storyDlBtnClassName}`, function(e) {
                if (isRepeatedClick(e)) {
                    return;
                }
                Extension.Stories.onClickDownloadSingleStory.call(this, e);
            });
            $body.on('click', `.${Extension.Stories.storyBulkDlBtnClassName}`, function(e) {
                if (isRepeatedClick(e)) {
                    return;
                }
                Extension.Stories.onClickBulkDownloadStories(e);
            });
        },
        messagesListenerStart: function() {
            chrome.runtime.onMessage.addListener(function(message, sender, cb) {
                if (!message) {
                    return false;
                }
                if (message == 'isBulkDownloadInTabNow') {
                    cb(Extension.bulkDownloadModule.processIsActive);
                }
            });
        },
        onClickDownloadBtn: function(e) {

            var el = this;

            Extension.loaderBtnState.on(el);
            if (el.dataset.simple_pt == '1') {
                var url = gb.getLinkFromHtml(el.parentNode);
                if (url) {
                    var userName = $(el).closest('article').find('header > div + div a').attr('href').replace(/\//g, '') || null;
                    var filename = gb.getFileNameFromLink(url, userName);
                    var photoVideoInfo = {
                        url: url,
                        filename: filename || null,
                    };
                    return Extension.whenGetPhotoVideoInfo(photoVideoInfo);
                }
            }

            var postIdentifier = el.dataset.identifier;

            gb.getMediaItemData({
                postIdentifier: postIdentifier,
                posInMultiple: el.dataset['simple_pt'] == 4 ? Extension.getMultiplePosition($(el).closest('article').get(0)) : undefined,
                isIgtv: gb.isIgtvPost(el.parentElement),
                cyclecount: 999, //is this evenn
                totalpostcount: 10
            }, function(photoVideoInfo) {
                Extension.whenGetPhotoVideoInfo(photoVideoInfo);
            }); // 1 cyclecount and 1 totalpostcount
        },
        whenGetPhotoVideoInfo: function(info) {
            var self = this;
            self.loaderBtnState.off();
            Extension.addOrUpdateCache(info);
            if (info && info.url) {
                gb.downloadFile(info, function() {});
            }
        },
        appendDlBtn: function(container, pageType) {
            if (container.querySelector(`.${Extension.dlBtnClassName}`)) {
                return;
            }
            var identifierContainerEl, postIdentifier;
            switch (pageType) {
                case 1:
                case 4:
                    identifierContainerEl = $(container).closest('article').get(0);
                    if (!identifierContainerEl) {
                        return;
                    }
                    var url = gb.getLinkFromHtml(container);
                    if (url) {
                        var userName = $(identifierContainerEl).find('header > div + div a').attr('href').replace(/\//g, '') || null;
                        var filename = gb.getFileNameFromLink(url, userName);
                    }
                    break;
                case 2:
                case 5:
                    identifierContainerEl = container;
                    break;
                case 3:
                    identifierContainerEl = container.querySelector('.EmbeddedMedia');
                    break;
                default:
                    return;
            }
            postIdentifier = Extension.getPostIdentifier(identifierContainerEl);
            if (!postIdentifier) return;
            if (pageType == 5) {
                container.style.position = 'relative';
            }
            var photoVideoInfo = {
                identifier: postIdentifier,
                page_type: pageType,
            };
            if (url) {
                photoVideoInfo.url = url;
                photoVideoInfo.filename = filename;
            }
            this.addOrUpdateCache(photoVideoInfo);
            this.$getDlBtnEl(photoVideoInfo).appendTo(container);
            container.dataset.simpleDone = '1';
        },
        $getDlBtnEl: function(info) {
            return $(`<a class="${Extension.dlBtnClassName}" type="button">
<span class="${cssPrefix}icon"></span>
<span class="${cssPrefix}text">${chrome.i18n.getMessage('download').toUpperCase()}</span>
</a>`).attr('data-identifier', info.identifier).attr('data-simple_pt', info.page_type);
        },
        getPostIdentifier: function(mediaContainer) {
            if (mediaContainer.tagName.toLowerCase() == 'a') {
                var $a = $(mediaContainer);
            } else {
                $a = $('a[href^="/p/"], a[href^="/tv/"]', mediaContainer);
            }
            var postIdentifier = $a.attr('href').match(new RegExp('\\/(p|tv)\\/([^/]+)'));
            return postIdentifier && postIdentifier[2];
        },
        getAuthorPageSector: function() {
            if (location.href.indexOf('/tagged/') > -1) {
                return 'tagged';
            } else if (location.href.indexOf('/saved/') > -1) {
                return 'saved';
            } else if (location.href.indexOf('/channel/') > -1) {
                return 'igtv';
            }
            return Extension.authorPageSector || 'main';
        },
        updateAuthorPageSector: function(clickBtn) {
            if (!clickBtn) {
                if (location.href.indexOf('/tagged/') > -1) {
                    Extension.authorPageSector = 'tagged'
                } else if (location.href.indexOf('/saved/') > -1) {
                    Extension.authorPageSector = 'saved'
                } else if (location.href.indexOf('/channel/') > -1) {
                    Extension.authorPageSector = 'igtv';
                } else {
                    Extension.authorPageSector = 'main';
                }
            } else {
                if (clickBtn.href.indexOf('channel') > -1) {
                    Extension.authorPageSector = 'igtv';
                } else if (clickBtn.href.indexOf('tagged') > -1) {
                    Extension.authorPageSector = 'tagged';
                } else if (clickBtn.href.indexOf('saved') > -1) {
                    Extension.authorPageSector = 'saved';
                } else {
                    Extension.authorPageSector = 'main'
                }
            }
            Extension.resetFoundLinks();
        },
        getCachedMedia: function() {
            Extension.authorPageSector = Extension.authorPageSector || Extension.getAuthorPageSector();
            return Extension.cachedMedia[Extension.authorPageSector];
        },
        resetFoundLinks: function() {
            this.cachedMedia = {
                main: [],
                tagged: [],
                saved: [],
                igtv: [],
            };
            this.cachedMediaIdentifiers = [];
        },
        getCachedLinkByIdentifier: function(postIdentifier) {
            if (Extension.cachedMediaIdentifiers.indexOf(postIdentifier) == -1) return null;
            var cachedMedia = Extension.getCachedMedia();
            for (var i = 0; cachedMedia[i]; i++) {
                if (cachedMedia[i].identifier == postIdentifier) {
                    if (typeof cachedMedia[i].url == 'string' && cachedMedia[i].url.length) {
                        return {
                            url: cachedMedia[i].url,
                            filename: cachedMedia[i].filename
                        }
                    }
                    break;
                }
            }
            return null;
        },
        addOrUpdateCache: function(photoVideoInfo) {
            if (!photoVideoInfo || !photoVideoInfo.identifier) {
                return;
            }
            var cachedMedia = Extension.getCachedMedia();
            if (Extension.cachedMediaIdentifiers.indexOf(photoVideoInfo.identifier) == -1) {
                cachedMedia.push(photoVideoInfo);
                Extension.cachedMediaIdentifiers.push(photoVideoInfo.identifier);
            } else if (photoVideoInfo.url) {
                for (var i = 0; cachedMedia[i]; i++) {
                    if (cachedMedia[i].identifier == photoVideoInfo.identifier) {
                        if (!cachedMedia[i].url) {
                            cachedMedia[i].url = photoVideoInfo.url;
                            cachedMedia[i].filename = photoVideoInfo.filename;
                        }
                        break;
                    }
                }
            }
        },
        getFoundIdentifiers: function(start, end) {
            start = typeof start != 'undefined' && !isNaN(parseInt(start)) ? (parseInt(start) - 1) : 0;
            end = !isNaN(parseInt(end)) ? parseInt(end) : undefined;
            return Extension.cachedMediaIdentifiers.slice(start, end);
        },
        getMultiplePosition: function(parent) {
            if (!parent) return 0;
            var divs = parent.querySelectorAll('header + div + div > div div');
            var i = 0;
            var points
            while (divs[i]) {
                var el = divs[i];
                // var yeahthatcyclecount = i;
                i++;
                if (el.offsetHeight < 10 && el.offsetHeight === el.offsetWidth) {
                    if (el.parentElement.offsetWidth > el.parentElement.offsetHeight * 20) {
                        var allChildrenContainerCandidates = el.parentElement.children;
                        var allChildrenMatch = true;
                        allChildrenContainerCandidates.forEach(function(childEl) {
                            if (!(childEl.offsetHeight < 10 && childEl.offsetHeight === childEl.offsetWidth)) {
                                allChildrenMatch = false;
                            }
                        });
                        if (allChildrenContainerCandidates.length > 1 && allChildrenMatch) {
                            points = allChildrenContainerCandidates;
                            break;
                        }
                    }
                }
            }
            var pos = 0;
            var classesCount = 0;
            points.forEach(function(el, key) {
                if (el.classList.length > classesCount) {
                    classesCount = el.classList.length;
                    pos = key;
                }
            })
            return pos;
        },
        modalInit: function() {
            var modalX = document.querySelector(`.${cssPrefix}modal`);
            if (!modalX) {
                modalX = document.createElement('div');
                modalX.className = `${cssPrefix}modal`;
                modalX.innerHTML = `<div class="${cssPrefix}modal_content"></div>`;
                document.querySelector('body').appendChild(modalX);
                modalX.opened = 0;
            }
            var btnContinue, btnCancel, btnEnough, closeBtn, progressBar, progressText;
            var allListeners = [];

            function modalClose() {
                removeListeners();
                modalX.opened = 0;
                modalX.remove();
            }

            function onContinueBtnClick(cb) {
                return function() {
                    modalClose();
                    cb();
                }
            }

            function onEnoughBtnClick(cb) {
                return function() {
                    modalClose();
                    cb();
                }
            }

            function onCancelBtnClick(cb) {
                return function() {
                    modalClose();
                    cb && cb();
                };
            }

            function addListener(el, event, listener) {
                if (el && event && listener) {
                    el.addEventListener(event, listener);
                    allListeners.push({
                        el: el,
                        event: event,
                        listener: listener
                    });
                }
            }

            function removeListeners() {
                allListeners.forEach(function(item) {
                    if (item.el && item.event && item.listener) {
                        item.el.removeEventListener(item.event, item.listener);
                    }
                });
            }

            function addListeners(listeners) {
                closeBtn = modalX.querySelector(`.${cssPrefix}modal_close_button`);
                btnCancel = modalX.querySelector(`.${cssPrefix}modal_button_cancel`);
                btnContinue = modalX.querySelector(`.${cssPrefix}modal_button_continue`);
                btnEnough = modalX.querySelector(`.${cssPrefix}modal_button_enough`);
                if (listeners) {
                    if (typeof listeners.continueCallback == 'function' && btnContinue) {
                        addListener(btnContinue, 'click', onContinueBtnClick(listeners.continueCallback));
                    }
                    if (typeof listeners.enoughCallback == 'function' && btnEnough) {
                        addListener(btnEnough, 'click', onEnoughBtnClick(listeners.enoughCallback));
                    }
                }
                addListener(closeBtn, 'click', onCancelBtnClick(listeners && listeners.cancelCallback));
                addListener(btnCancel, 'click', onCancelBtnClick(listeners && listeners.cancelCallback));
            }

            function alignButtonsWidth() {
                modalX.style.display = 'block';
                setTimeout(function() {
                    var maxWidth = 0;
                    var $buttons = $(`.${cssPrefix}modal_buttons_wrap button`);
                    $buttons.each(function(i, item) {
                        if (item.offsetWidth > maxWidth) {
                            maxWidth = item.offsetWidth;
                        }
                    });
                    if (maxWidth > 0) {
                        $buttons.css({
                            width: maxWidth + 'px',
                        })
                    }
                    $(`.${cssPrefix}modal_content`).css({
                        opacity: 1
                    });
                    modalX.opened = 1;
                }, 100);
            }
            modalX.close = function() {
                modalClose();
            };
            modalX.prepareDownload = function(options, cb) {
                var progressBarStart = options.bar_start || 0;
                var modalContent = modalX.querySelector(`.${cssPrefix}modal_content`);
                modalContent.innerHTML = `<div class="${cssPrefix}modal_header">
<span>${chrome.i18n.getMessage('preparing')}...</span>
<div class="${cssPrefix}_counter"></div>
</div>
<div id="${cssPrefix}progress_line" ${(options.from_tagged ? 'class="inst_dl_simple_circle_progress"' : '')}>
<div id="${cssPrefix}progress_bar"></div>
</div>
<div class="${cssPrefix}modal_buttons_wrap ${cssPrefix}one_btn_inside">
<button class="${cssPrefix}modal_button_cancel">${chrome.i18n.getMessage('cancel').toUpperCase()}</button>
</div>
<div class="${cssPrefix}modal_footer">
<div class="${cssPrefix}dont_panic" style="display: none">'
<span>${chrome.i18n.getMessage('waiting_response_for_virtual_scroll')}</span>
<span class="${cssPrefix}loader"></span>
</div>
</div>`;
                addListeners(cb);
                progressText = modalContent.querySelector(`.${cssPrefix}_counter`);
                progressBar = modalX.querySelector(`#${cssPrefix}progress_bar`);
                progressBar.style.width = progressBarStart + '%';
                if (options.from_tagged) {
                    modalContent.style.maxWidth = '300px';
                    modalContent.querySelector(`.${cssPrefix}modal_button_cancel`).style.width = '100%';
                } else {
                    modalContent.style.width = '520px';
                }
                alignButtonsWidth();
            };
            modalX.showDownloadProcess = function(opts, cb) {
                var modalContent = modalX.querySelector(`.${cssPrefix}modal_content`);
                modalContent.innerHTML = `<div class="${cssPrefix}modal_header">
<span>${chrome.i18n.getMessage('downloading')}...</span>
<div class="${cssPrefix}_counter"></div>
</div>
<div id="${cssPrefix}progress_line">
<div id="${cssPrefix}progress_bar"></div>
</div>
<div class="${cssPrefix}modal_buttons_wrap ${cssPrefix}align_right">
<button class="${cssPrefix}modal_button_enough">${chrome.i18n.getMessage('stop_and_save').toUpperCase()}</button>
<button class="${cssPrefix}modal_button_cancel">${chrome.i18n.getMessage('cancel').toUpperCase()}</button>
</div>`;
                progressText = modalContent.querySelector(`.${cssPrefix}_counter`);
                progressBar = modalX.querySelector(`#${cssPrefix}progress_bar`);
                progressText.innerText = '0/' + opts.files_count + ' ' + chrome.i18n.getMessage('files');
                modalContent.style.width = '520px';
                addListeners(cb);
                alignButtonsWidth();
            };
            modalX.showZippingLoader = function(cb) {
                var modalContent = modalX.querySelector(`.${cssPrefix}modal_content`);
                modalContent.innerHTML = `<div class="${cssPrefix}modal_header">
<span>${chrome.i18n.getMessage('zipping_files')}...</span>
<div class="${cssPrefix}_counter"></div>
</div>
<div id="${cssPrefix}progress_line">
<div id="${cssPrefix}progress_bar"></div>
</div>
<div class="${cssPrefix}modal_buttons_wrap ${cssPrefix}one_btn_inside">
<button class="${cssPrefix}modal_button_cancel ">${chrome.i18n.getMessage('cancel').toUpperCase()}</button>
</div>`;
                progressText = modalContent.querySelector(`.${cssPrefix}_counter`);
                progressText.innerText = '0%';
                progressBar = modalX.querySelector(`#${cssPrefix}progress_bar`);
                progressBar.style.width = '0%';
                modalContent.style.width = '520px';
                addListeners(cb);
            };
            modalX.updatePrepareDownload = function(options) {
                if (options.from_tagged) {
                    if (options.counter_text) {
                        $(`.${cssPrefix}_counter`).text(options.counter_text);
                    }
                    return;
                }
                var $doNotPanic = $('.ext_dont_panic');
                if (options.dont_panic) {
                    $doNotPanic.show();
                } else {
                    $doNotPanic.hide();
                }
                if (options.no_progress) {
                    return;
                }
                var value = options.oneValuePercent;
                var maxVal = options.maxValue || 100;
                if (!progressBar.style.width) {
                    progressBar.style.width = '0%';
                }
                var oldVal = parseFloat(progressBar.style.width);
                var newVal = oldVal + value;
                if (newVal > maxVal) {
                    newVal = maxVal;
                }
                progressBar.style.width = newVal + '%';
            };
            modalX.updateProgressBar = function(options) {
                if (options.from_tagged) return;
                progressText.innerText = options.successCount.toString() + '/' + options.allCount.toString() + ' ' + chrome.i18n.getMessage('files');
                var value = options.oneValuePercent;
                if (!progressBar.style.width) {
                    progressBar.style.width = '0%';
                }
                var oldVal = parseFloat(progressBar.style.width);
                var newVal = oldVal + value;
                if (newVal == 100 || newVal > 100) {
                    newVal = 100;
                }
                progressBar.style.width = newVal + '%';
            };
            modalX.updateZippingLoader = function(newVal) {
                progressText.innerText = newVal + '%';
                progressBar.style.width = newVal + '%';
            };
            modalX.showErrorBox = function(errorText) {
                var modalContent = modalX.querySelector(`.${cssPrefix}modal_content`);
                modalContent.innerHTML = `<span class="${cssPrefix}modal_close_button">&times;</span>
<div class="${cssPrefix}error_modal_wrap">${errorText}</div>`;
                addListeners();
                alignButtonsWidth();
            };
            return modalX;
        },
    };
    Extension.run();
})();