! function() {
    window.simpleGb = {
        queryHashesFailed429: 0,
        queryHashesFailed500: 0,
        queryHashesFailed560: 0,
        workingQueryHashOwner: "003056d32c2554def87228bc3fd9668a",
        workingQueryHashSaved: "2ce1d673055b99250e93b6f88f878fde",
        workingQueryHashTagged: "31fe64d9463cbbe58319dced405c6206",
        workingQueryHashStoriesHighlight: "d4d88dc1500312af6f937f7b804c68c3",
        xIgAppId: "936619743392459",
        xIgWwwClaim: "0",
        firstParamRequestJSONgraphqlQuery: 48,
        getMediaItemData: function(a, b) {
            this.requestPostDataByIdentifier(a, function(c) {
                var d, e = null;
                try {
                    var f = c.owner.username,
                        g = c.location && c.location.slug;
                    d = c.edge_sidecar_to_children && c.edge_sidecar_to_children.edges && c.edge_sidecar_to_children.edges.length ? c.edge_sidecar_to_children.edges[a.posInMultiple || 0].node : c, e = simpleGb.takeAwayMediaItemData(d, f, g)
                } catch (h) {}
                b(e)
            }, a.cyclecount, a.totalpostcount)
            
            //parsecount = parsecount +1;
        },
        takeAwayMediaItemData: function(a, b, c) {
            var d, e, f;
            try {
                if (a.is_video) d = a.video_url, e = this.getFileNameFromLink(d, b, c), f = a.product_type;
                else {
                    if (d = a.display_url, !d) {
                        var g = 0,
                            h = 0;
                        a.display_resources.forEach(function(a) {
                            (a.config_width > g || a.config_height > h) && (g = a.config_width, h = a.config_height, d = a.src)
                        })
                    }
                    e = this.getFileNameFromLink(d, b, c)
                }
            } catch (i) {}
            return {
                url: d,
                filename: e,
                product_type: f
            }
        },
        getUserFirstPagePostsData: function(a, b) {
            simpleGb.requestAuthorInfo(a.userName, function(c) {
                try {
                    if (a.fromSaved) var d = c.graphql.user.edge_saved_media;
                    else {
                        if (a.fromTagged) {
                            var e = c.graphql.user.id;
                            return b({
                                userId: e
                            })
                        }
                        d = c.graphql.user.edge_owner_to_timeline_media
                    }
                    var f = simpleGb.collectIdentifiersFromPostsEdge({
                        media: d,
                        username: a.userName
                    });
                    f.userId = c.graphql.user.id, f.count = d.count
                } catch (g) {
                    return b({
                        error: 1
                    })
                }
                b(f)
            })
        },
        sleep: function (milliseconds) {
            var start = new Date().getTime();
            for (var i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds){
            break;
            }
            }
        },
        getWholePostData: function(a, b, millisecdelay, cyclecount, totalpostcount) {
            //console.log("testing 123");
            // console.log("millisecdelay in getWholePostData: is "+millisecdelay);
            // console.log(Object.keys(millisecdelay));
            // console.log(Object.values(millisecdelay));
            // msecoutput = String(Object.values(millisecdelay));
            // simpleGb.sleep(9000);
            // console.log("go 9000");
            // parsecount = 3;

            //defined in Line 735 of content.js
            // console.log("gwpd cyclecount is "+a.cyclecount);
            // console.log("gwpd cyclecount is "+a.cyclecount);
            // console.log("gwpd totalpostcount is "+a.totalpostcount);

            return this.requestPostDataByIdentifier(a, function(a) {
                if (a && a.error) return b(a);
                try {
                    var c = [],
                        d = function(b) {
                            var d = simpleGb.takeAwayMediaItemData(b, a.owner.username);
                            d && d.url && c.push(d)
                        };
                    a.edge_sidecar_to_children && a.edge_sidecar_to_children.edges ? a.edge_sidecar_to_children.edges.forEach(function(a) {
                        d(a.node)
                    }) : d(a), b(c)
                } catch (e) {
                    return b({
                        error: 1
                    })
                }}
            ,
            millisecdelay, cyclecount
            )
        },
        getPostsDataByQueryHash: function(a, b) {

            simpleGb.requestPostsByQueryHash(a, function(c) {
                try {
                    if (a.from_saved) var d = c.data.user.edge_saved_media;
                    else if (a.from_tagged) d = c.data.user.edge_user_to_photos_of_you;
                    else {
                        if (a.highlight_reel_id) {
                            d = c.data.reels_media;
                            var e = simpleGb.collectStoriesFromGraphql(d);
                            return b(e)
                        }
                        d = c.data.user.edge_owner_to_timeline_media
                    }
                } catch (f) {
                    return b()
                }
                e = simpleGb.collectIdentifiersFromPostsEdge({
                    media: d,
                    username: a.user_name
                }), b(e)
            })
        },
        requestAuthorInfo: function(a, b) {
            var c = "https://www.instagram.com/" + a + "/?__a=1";
            $.ajax({
                url: c,
                dataType: "json",
                headers: {
                    "x-ig-app-id": simpleGb.xIgAppId
                }
            }).done(b).fail(function() {
                b()
            })
        },
        requestPostsByQueryHash: function(a, b) {

            // simpleGb.sleep(9000);
            // console.log("XXXX wait for "+millisecdelay+"ms before fetching next media item");
            
            if (a.bulkDownloadObject && (a.bulkDownloadObject.bulkCanceled || a.bulkDownloadObject.processId !== a.process_id)) return b({
                error: 1
            });
            var c, d, e = "string" == typeof a.highlight_reel_id;
            if (e) d = {
                reel_ids: [],
                tag_names: [],
                location_ids: [],
                highlight_reel_ids: [a.highlight_reel_id],
                precomposed_overlay: !1
            }, c = simpleGb.workingQueryHashStoriesHighlight;
            else {
                var f = a.first || simpleGb.firstParamRequestJSONgraphqlQuery;
                d = {
                    id: a.user_id,
                    first: f
                }, a.end_cursor && (d.after = a.end_cursor), c = a.from_saved ? simpleGb.workingQueryHashSaved : a.from_tagged ? simpleGb.workingQueryHashTagged : simpleGb.workingQueryHashOwner
            }
            var g = "https://www.instagram.com/graphql/query/?query_hash=" + c + "&variables=" + encodeURI(JSON.stringify(d));
            $.ajax({
                url: g,
                dataType: "json"
            }).done(function(a) {
                var c;
                if ("object" != typeof a) try {
                    c = JSON.parse(a)
                } catch (d) {} else c = a;
                return c || c.data ? void b(c) : b({
                    error: 1
                })
            }).fail(function(c) {
                if (429 == c.status) {
                    if (a.touch || simpleGb.queryHashesFailed429 > 5) return simpleGb.queryHashesFailed429 = 0, b({
                        error: 1
                    });
                    simpleGb.queryHashesFailed429++, setTimeout(function() {
                        simpleGb.requestPostsByQueryHash(a, b)
                    }, 12e4 * simpleGb.queryHashesFailed429)
                } else if (560 == c.status) {
                    if (simpleGb.queryHashesFailed560 > 2) return simpleGb.queryHashesFailed560 = 0, b({
                        error: 1
                    });
                    simpleGb.queryHashesFailed560++, setTimeout(function() {
                        simpleGb.requestPostsByQueryHash(a, b)
                    }, 2e3 * simpleGb.queryHashesFailed560)
                } else {
                    if (500 != c.status) return b({
                        error: 1
                    });
                    if (simpleGb.queryHashesFailed500 > 5) return simpleGb.queryHashesFailed500 = 0, b({
                        error: 1
                    });
                    simpleGb.queryHashesFailed500++, setTimeout(function() {
                        simpleGb.requestPostsByQueryHash(a, b)
                    }, 3e3 * simpleGb.queryHashesFailed429)
                }
            })
            // simpleGb.sleep(9000);
            // console.log("wait for "+millisecdelay+"ms before fetching next media item");
        },
        requestPostDataByIdentifier: function(a, b, millisecdelay, cyclecount, totalpostcount) {
            a = a || {};
            var c = a.postIdentifier;

            // console.log("cyclecount is "+cyclecount);
            // console.log("totalpostcount is "+totalpostcount);

            // console.log ("cyclecount in rPDBY: is "+cyclecount);
            // console.log ("cyclecount in rPDBY: is "+totalpostcount);

            // console.log("post permalink: "+c);
            //console.log(Object.keys(c));
            //console.log(Object.values(c));
            //insert delay here
            // desbest edit
           // console.log(millisecdelay);
            // console.log(Object.keys(millisecdelay));
            // console.log(Object.values(millisecdelay));

            // simpleGb.sleep(9000); console.log("wait 9 seconds");

            if ("string" == typeof c && c.length && "function" == typeof b) {
                // if(!parsecount){ parsecount = 1}
                // parsecount = parsecount + 1;
                if (!a.isIgtv) return this.requestPostDataByIdentifier2(a, b, millisecdelay, cyclecount, totalpostcount);
                var d = "https://www.instagram.com/tv/" + c + "/?__a=1";
                //insert wait here

                return $.ajax({
                    url: d,
                    dataType: "json",
                    headers: {
                        "x-ig-app-id": simpleGb.xIgAppId,
                        "x-ig-www-claim": simpleGb.xIgWwwClaim
                    }
                }).done(function(a) {
                    var c;
                    if ("object" != typeof a) try {
                        c = JSON.parse(a)
                    } catch (d) {} else c = a;
                    return c && c.graphql && c.graphql.shortcode_media ? void b(c.graphql.shortcode_media) : b({
                        error: 1
                    })
                }).fail(function(a) {
                    return b(429 == a.status ? {
                        error: 1,
                        reason: 429
                    } : {
                        error: 1
                    })
                })
            }
        },
        requestPostDataByIdentifier2: function(a, b, millisecdelay, cyclecount, totalpostcount) {
            //alert("test print");
            // simpleGb.sleep(9000);

            //if(totalpostcount > 1){
            // console.log(millisecdelay);
                // console.log(Object.keys(millisecdelay));
                // console.log(Object.values(millisecdelay));
                msecoutput = String(Object.values(millisecdelay));
                postID = String(Object.values(a.postIdentifier));
                
                cyclecount = sessionStorage.getItem('cyclecount', 1);
                console.log("wait "+msecoutput+"ms while fetching post "+a.postIdentifier+" --//-- post "+cyclecount+"/"+a.totalpostcount);
                simpleGb.sleep(millisecdelay);

                // notice how the "<25" remains consistent, throughout
                //an added 3/12 second pause for 25 posts
                    //since each 200/1050 posts
                if((cyclecount % 200) < 25){
                    simpleGb.sleep(192000);
                    console.log("----> an extra 192sec pause for every 25 posts since each 200 posts");
                }

                if((cyclecount % 400) < 25){
                    simpleGb.sleep(192000);
                    console.log("--------> an extra 192sec pause for every 25 posts since each 400 posts");
                }

                if((cyclecount % 1050) < 25 && cyclecount > 1000){
                    simpleGb.sleep(192000);
                    console.log("------------> an extra 192sec pause for every 25 posts since each 1050 posts");
                }

                if((cyclecount % 750) < 25){
                    simpleGb.sleep(192000);
                    console.log("--------> an extra 192sec pause for every 25 posts since each 750 posts");
                }

                if((cyclecount % 600) < 25){
                    simpleGb.sleep(192000);
                    console.log("--------> an extra 192sec pause for every 25 posts since each 600 posts");
                }
            //} // useless totalcount check

            this.sendMessage("getCsrftoken", function(c) {
                var d = "https://www.instagram.com/graphql/query/?query_hash=2c4c2e343a8f64c625ba02b2aa12c7f8&variables=" + encodeURIComponent(JSON.stringify({
                        shortcode: a.postIdentifier
                    })),
                    e = {
                        "x-ig-app-id": simpleGb.xIgAppId
                    };

                //exit the rPDBI-->2 function once "return"--> runs
                // provide the required data using return to the parent

                return c && (e["x-csrftoken"] = c), 
                    $.ajax({
                    url: d,
                    dataType: "json",
                    headers: e
                }).done(function(a) {

                    //firstly, check for instagram's bot challenge
                    if(d.indexOf("instagram.com/challenge") !== -1) {
                        return b("botchallenge" == a.status ? {
                            error: 1,
                            reason: "botchallenge",
                            botchallengeurl: d
                        } : {
                            error: 1
                        })
                    }

                    var c;
                    if ("object" != typeof a) try {
                        c = JSON.parse(a)
                    } catch (d) {} else c = a;
                    return c && c.data && c.data.shortcode_media ? void b(c.data.shortcode_media) : b({
                        error: 1
                    })
                }).fail(function(a) {
                    return b(429 == a.status ? {
                        error: 1,
                        reason: 429
                    } : {
                        error: 1
                    })
                })
            icount = icount +1;
            })
        },
        collectStoriesFromGraphql: function(a) {
            var b = [];
            return a[0].items.forEach(function(a) {
                try {
                    var c, d, e, f = 0,
                        g = 0,
                        h = a.is_video ? a.video_resources : a.display_resources;
                    if (!Array.isArray(h)) return;
                    if (h.forEach(function(a) {
                            try {
                                (a.config_width > f || a.config_height > g) && (f = a.config_width, g = a.config_height, c = a.src)
                            } catch (b) {}
                        }), !c) return;
                    e = a.owner.username, d = simpleGb.getFileNameFromLink(c), b.push({
                        url: c,
                        filename: d,
                        username: e
                    })
                } catch (i) {}
            }), b
        },
        collectIdentifiersFromPostsEdge: function(a) {
            var b = a.media,
                c = a.username,
                d = [],
                e = !1,
                f = null,
                g = !1;
            try {
                var h = b.edges;
                h.length && h.forEach(function(a) {
                    "igtv" != a.node.product_type && ("GraphImage" == a.node.__typename && a.node.display_url ? d.push({
                        url: a.node.display_url,
                        identifier: a.node.shortcode,
                        filename: simpleGb.getFileNameFromLink(a.node.display_url, c),
                        type: "photo"
                    }) : d.push({
                        identifier: a.node.shortcode
                    }))
                }), e = b.page_info.has_next_page, f = b.page_info.end_cursor
            } catch (i) {
                return {
                    error: 1
                }
            }
            return {
                identifiers: d,
                end_cursor: f,
                has_next_page: e,
                time_end: g,
                count: b.count
            }
        },
        isVideoPost: function(a) {
            return !!a.querySelector(["video", ".videoSpritePlayButton", ".coreSpriteVideoIconLarge", ".glyphsSpriteVideo_large"].join(","))
        },
        isImagePost: function(a) {
            return !!a.querySelector("img[src], img[srcset]")
        },
        isIgtvPost: function(a) {
            return !(!a.href || !a.href.match(new RegExp("\\/tv\\/([^/]+)")))
        },
        isStoriesActive: function() {
            return new RegExp("instagram\\.com\\/stories\\/[^/]+").test(window.location.href)
        },
        isIFrame: function() {
            return window.top != window.self
        },
        getLinkFromHtml: function(a) {
            if (simpleGb.isVideoPost(a)) {
                var b = simpleGb.findVideoEl(a);
                if (b) var c = simpleGb.getDirectLinkFromVideoEl(b)
            } else simpleGb.isImagePost(a) && (b = simpleGb.findImageEl(a), b && (c = simpleGb.getDirectLinkFromImgEl(b)));
            return c && simpleGb.isValidUrl(c) ? c : null
        },
        findVideoEl: function(a) {
            return a.querySelector("video")
        },
        findImageEl: function(a) {
            var b = null,
                c = a.querySelectorAll("img[src], img[srcset]");
            if (1 === c.length) b = c[0];
            else if (c.length > 1) {
                for (var d = 0; c[d]; d++) {
                    var e = c[d].getAttribute("src"),
                        f = c[d].getAttribute("srcset");
                    if ((e || f) && !("string" == typeof e && e.indexOf("chrome-extension") > -1) && ("string" == typeof e && e.indexOf("instagram") > -1 || "string" == typeof f && f.indexOf("instagram") > -1) && c[d].width > 100) {
                        b = c[d];
                        break
                    }
                }
                b || (b = c[0])
            }
            return b ? b : null
        },
        getDirectLinkFromVideoEl: function(a) {
            var b = a.getAttribute("src");
            if ("string" == typeof b && b.length) return b;
            var c = a.querySelectorAll("source");
            if (!c.length) return null;
            var d = ["avc1.64001E", "avc1.4D401E", "avc1.58A01E", "avc1.42E01E"],
                e = [];
            c.forEach(function(a) {
                var b = a.getAttribute("type");
                if (b) {
                    var c = b.match(new RegExp('codecs="([^,]+)'));
                    if (c = c && c[1]) {
                        var f = d.indexOf(c); - 1 != f && (e[f] = a.src)
                    }
                }
            });
            for (var f in e)
                if ("string" == typeof e[f] && e[f].length) {
                    b = e[f];
                    break
                }
            return b || (b = c[0].getAttribute("src")), b
        },
        getDirectLinkFromImgEl: function(a) {
            if (!(a instanceof HTMLElement)) return null;
            var b = a.getAttribute("srcset");
            if (b) {
                var c = {},
                    d = b.split(",");
                d.forEach(function(a) {
                    var b = a.split(" "),
                        d = b[1].replace(/[^\d]/, "");
                    c[d] || (c[d] = b[0])
                });
                var e = 0;
                for (var f in c) + f > +e && (e = f);
                var g = c[e]
            }
            return "string" == typeof g && g.match(new RegExp("\\.(jpg|png)")) || (g = a.getAttribute("src")), "string" != typeof g ? null : g
        },
        getFileNameFromLink: function(a, b, c) {
            if ("string" != typeof a) return null;
            var d = a.match(/\/([^\/?]+)(?:$|\?)/);
            return d = d && d[1], d ? (b && (d = b + "_" + d), d) : null
        },
        fixForeach: function() {
            "undefined" == typeof NodeList.prototype.forEach && (NodeList.prototype.forEach = Array.prototype.forEach), "undefined" == typeof HTMLCollection.prototype.forEach && (HTMLCollection.prototype.forEach = Array.prototype.forEach), "undefined" == typeof NodeList.prototype.filter && (NodeList.prototype.filter = Array.prototype.filter), "undefined" == typeof HTMLCollection.prototype.filter && (HTMLCollection.prototype.filter = Array.prototype.filter)
        },
        isValidUrl: function(a) {
            return !("string" != typeof a || -1 != a.indexOf("blob:") || !a.match(/\.(png|jpg|mp4|flv)/))
        },
        downloadFile: function(a, b) {
            return this.downloadByChrome(a, b)
        },
        downloadByChrome: function(a, b) {
            this.sendMessage({
                action: "downloadFile",
                options: {
                    url: a.url,
                    filename: a.filename,
                    isStory: "undefined" != typeof a.isStory
                }
            }, function(a) {
                "function" == typeof b && b(a)
            })
        },
        sendMessage: function(a, b) {
            window.insta_dl_disabled || ("undefined" == typeof b ? chrome.runtime.sendMessage(a) : chrome.runtime.sendMessage(a, b))
        },
        bridge: function(a) {
            a.args = a.args || [], void 0 === a.timeout && (a.timeout = 300);
            var b = "instasimple-bridge-" + parseInt(1e3 * Math.random()) + "-" + Date.now(),
                c = function(d) {
                    window.removeEventListener("instasimple-bridge-" + b, c);
                    var e;
                    e = d.detail ? JSON.parse(d.detail) : void 0, a.cb(e)
                };
            window.addEventListener("instasimple-bridge-" + b, c);
            var d = "(" + function(a, b, c, d) {
                    var e = document.getElementById(c);
                    e && e.parentNode.removeChild(e);
                    var f = !1,
                        g = function(a) {
                            if (!f) {
                                f = !0;
                                var b = new CustomEvent("instasimple-bridge-" + c, {
                                    detail: JSON.stringify(a)
                                });
                                window.dispatchEvent(b)
                            }
                        };
                    d && setTimeout(function() {
                        g()
                    }, d), b.push(g), a.apply(null, b)
                }.toString() + ")(" + [a.func.toString(), JSON.stringify(a.args), JSON.stringify(b), parseInt(a.timeout)].join(",") + ");",
                e = document.createElement("script");
            e.id = b, e.innerText = d, document.body.appendChild(e)
        },
        getUserNameFromWindowLocation: function() {
            if (window.location.href.indexOf("instagram.com/p/") > -1) return null;
            var a = window.location.href.match(/instagram\.com\/([^\/]+)/);
            return a && a[1].trim() || null
        },
        objectSortByProp: function(a, b, c) {
            function d(a, d) {
                var e = parseInt(a[b]),
                    f = parseInt(d[b]);
                return c ? f > e ? 1 : e > f ? -1 : 0 : e > f ? 1 : f > e ? -1 : 0
            }
            a.sort(d)
        },
        shuffle: function(a) {
            var b, c, d;
            for (d = a.length - 1; d > 0; d--) b = Math.floor(Math.random() * (d + 1)), c = a[d], a[d] = a[b], a[b] = c;
            return a
        },
        execTasksFlow: function(a, b) {
            function c() {
                f || (f = setInterval(function() {
                    g !== i && (clearInterval(f), f = void 0, g === j ? q() : r())
                }, 10))
            }
            var d, e, f, g, h, i = 0,
                j = 1,
                k = 2,
                l = a.data,
                m = a.asyncCount || 48,
                n = this,
                o = !1,
                p = [],
                q = function() {
                    "function" == typeof h.resolve && (l = p, setTimeout(function() {
                        try {
                            h.resolve(l)
                        } catch (a) {
                            console.error(a), p = a, r()
                        }
                    }, 0))
                },
                r = function() {
                    o || (o = !0, "function" == typeof h.reject && (b = h.reject)(p))
                },
                s = function() {
                    e = 0, d = l.length;
                    var a = 0,
                        f = function(b) {
                            return function(c) {
                                Array.isArray(p) && g != k && (a--, e++, p[b] = c, e === d && (g = j))
                            }
                        },
                        h = function(a) {
                            g = k, p = a
                        };
                    g = i, c();
                    var n = setInterval(function() {
                        if (!l.length || g != i) return void clearInterval(n);
                        var c = m - a;
                        1 > c || l.splice(0, c).forEach(function(c, d) {
                            setTimeout(function() {
                                try {
                                    a++, b(c, f(d), h)
                                } catch (e) {
                                    console.error(e), p = e, g = k
                                }
                            }, 0)
                        })
                    }, 500)
                };
            return this.thenOne = function(a, b) {
                return h = {
                    resolve: a,
                    reject: b
                }, c(), n
            }, s(), this
        },
        getStoriesViewer: function() {
            return document.querySelector("#react-root > section")
        },
        filename: {
            rtrim: /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
            illegalRe: /[\/\?<>\\:\*\|"~]/g,
            controlRe: /[\x00-\x1f\x80-\x9f]/g,
            reservedRe: /^\.+/,
            partsRe: /^(.+)\.([a-z0-9]{1,4})$/i,
            specialChars: "nbsp,iexcl,cent,pound,curren,yen,brvbar,sect,uml,copy,ordf,laquo,not,shy,reg,macr,deg,plusmn,sup2,sup3,acute,micro,para,middot,cedil,sup1,ordm,raquo,frac14,frac12,frac34,iquest,Agrave,Aacute,Acirc,Atilde,Auml,Aring,AElig,Ccedil,Egrave,Eacute,Ecirc,Euml,Igrave,Iacute,Icirc,Iuml,ETH,Ntilde,Ograve,Oacute,Ocirc,Otilde,Ouml,times,Oslash,Ugrave,Uacute,Ucirc,Uuml,Yacute,THORN,szlig,agrave,aacute,acirc,atilde,auml,aring,aelig,ccedil,egrave,eacute,ecirc,euml,igrave,iacute,icirc,iuml,eth,ntilde,ograve,oacute,ocirc,otilde,ouml,divide,oslash,ugrave,uacute,ucirc,uuml,yacute,thorn,yuml".split(","),
            specialCharsList: [
                ["amp", "quot", "lt", "gt"],
                [38, 34, 60, 62]
            ],
            specialCharsRe: /&([^;]{2,6});/g,
            rnRe: /\r?\n/g,
            re1: /[\*\?"]/g,
            re2: /</g,
            re3: />/g,
            spaceRe: /[\s\t\uFEFF\xA0]+/g,
            dblRe: /(\.|!|\?|_|,|\-|:|\+){2,}/g,
            re4: /[\.,:;\/\-_\+=']$/g,
            decodeUnicodeEscapeSequence: function(a) {
                var b = /\\(\\u[0-9a-f]{4})/g;
                try {
                    return JSON.parse(JSON.stringify(a).replace(b, "$1"))
                } catch (c) {
                    return a
                }
            },
            decodeSpecialChars: function(a) {
                var b = this;
                return a.replace(this.specialCharsRe, function(a, c) {
                    var d = null;
                    if ("#" === c[0]) return d = parseInt(c.substr(1)), isNaN(d) ? "" : String.fromCharCode(d);
                    var e = b.specialCharsList[0].indexOf(c);
                    return -1 !== e ? (d = b.specialCharsList[1][e], String.fromCharCode(d)) : (e = b.specialChars.indexOf(c), -1 !== e ? (d = e + 160, String.fromCharCode(d)) : "")
                })
            },
            trim: function(a) {
                return a.replace(this.rtrim, "")
            },
            getParts: function(a) {
                return a.match(this.partsRe)
            },
            modify: function(a) {
                if (!a) return "";
                a = this.decodeUnicodeEscapeSequence(a);
                try {
                    a = decodeURIComponent(a)
                } catch (b) {
                    a = unescape(a)
                }
                if (a = this.decodeSpecialChars(a), a = a.replace(this.rnRe, " "), a = this.trim(a), a = a.replace(this.re1, "").replace(this.re2, "(").replace(this.re3, ")").replace(this.spaceRe, " ").replace(this.dblRe, "$1").replace(this.illegalRe, "_").replace(this.controlRe, "").replace(this.reservedRe, "").replace(this.re4, ""), a.length <= this.maxLength) return a;
                var c = this.getParts(a);
                return c && 3 == c.length ? (c[1] = c[1].substr(0, this.maxLength), c[1] + "." + c[2]) : a
            }
        }
    }
}();